## Problem Statement
At Shopify, merchants utilize a variety of fulfillment configurations which are passed to the Shopify backend for further processing.

Consider a scenario where a buyer placed an order from a merchant which contains the following items, a laptop, a keyboard and a mouse.

The merchant's fulfillment configuration, based on this order, generated two rules like this: `[["laptop", "keyboard"], ["keyboard", "mouse"]]`. This rule communicates to Shopify that for this order, the "laptop" and "keyboard" must be shipped together, and similarly, the "keyboard" and "mouse" must also be shipped together. The only way to satisfy both rules is to ship all of these items together. Hence, the rule can be simplified to `[["laptop", "keyboard", "mouse"]]`.

Can you assist us in developing a logic to consolidate these rules?

For simplicity, let's use letters to represent items for the following examples.

Example 1:
```ruby
# input
rules = [
    ["A", "B"],
    ["B", "C"],
    ["C", "D"]
]

# output
consolidated_rules = [["A", "B", "C", "D"]]
```

Example 2:
```ruby
# input
rules = [
    ["A", "B"],
    ["C", "D"],
    ["D", "E"],
    ["F", "G"]
]

# output
consolidated_rules = [
    ["A", "B"],
    ["C", "D", "E"],
    ["F", "G"]
]
```

Example 3:
```ruby
# input
rules = [
    ["A", "B", "C"],
    ["E", "F", "G"],
    ["J", "K"],
    ["C", "D", "E"],
]

# output
consolidated_rules = [
    ["A", "B", "C", "D", "E", "F", "G"],
    ["J", "K"],
]
```

Example 4:
```ruby
# input
rules = [
    ["A", "B", "C", "C"],
    [],
    ["A", "D", "D"],
    ["D", "E"],
    ["F", "G"],
]

# output
consolidated_rules = [
    ["A", "B", "C", "D", "E"],
    ["F", "G"],
]
```
