package org.kurczynski;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        System.out.println("Ahoy hoy!");
    }

    public List<List<String>> consolidate(List<List<String>> input) {
        List<Set<String>> consolidated = new ArrayList<>();

        for (var order : input) {
            System.out.println("Order: " + order);
            Set<String> matches = new HashSet<>();

            for (var item : order) {
                var output = orderContains(input, item);
                matches.addAll(output);
            }

            consolidated.add(matches);

            System.out.println(matches);
        }

        System.out.println("Consolidated first pass: " + consolidated);
        List<List<String>> consolidatedOutput = new ArrayList<>();

        for (var c : consolidated) {
            Set<String> matches = new HashSet<>();

            for (var item : c) {
                var output = orderContains(input, item);
                matches.addAll(output);
            }

            if (!matches.isEmpty()) {
                consolidatedOutput.add(new ArrayList<>(matches));
            }

            System.out.println("Matches: " + matches);
        }

        var myUniqueList = consolidatedOutput.stream().distinct().toList();
        System.out.println("Consolidated: " + myUniqueList);

        return myUniqueList;
    }

    public Set<String> orderContains(List<List<String>> input, String item) {
        List<List<String>> output = new ArrayList<>();
        Set<String> store = new HashSet<>();

        for (var order : input) {
            if (order.contains(item)) {
                store.addAll(order);
            }
        }

        return store;
    }
}
