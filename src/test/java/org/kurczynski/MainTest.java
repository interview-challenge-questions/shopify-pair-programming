package org.kurczynski;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

class MainTest {
    private static void assertRulesEqual(List<List<String>> expected, List<List<String>> actual) {
        var areEqual = false;

        for (var i : expected) {
            for (var j : actual) {
                if (i.containsAll(j)) {
                    areEqual = true;
                } else {
                    areEqual = false;

                    break;
                }
            }

            if (!areEqual) {
                break;
            }
        }

        if (areEqual) {
            assertTrue(true);
        } else {
            System.out.format("Expected: %s\nActual: %s\n", expected, actual);
            fail("Rules do not match");
        }
    }

    @Test
    public void shouldConsolidateOne() {
        final var expected = List.of(
                List.of("A", "B", "C", "D")
        );

        Main main = new Main();

        List<String> orderOne = List.of("A", "B");
        List<String> orderTwo = List.of("B", "C");
        List<String> orderThree = List.of("C", "D");

        List<List<String>> input = new ArrayList<>();

        input.add(orderOne);
        input.add(orderTwo);
        input.add(orderThree);

        var actual = main.consolidate(input);

        assertRulesEqual(expected, actual);
    }

    @Test
    public void shouldConsolidateTwo() {
        Main main = new Main();

        List<String> orderOne = List.of("A", "B");
        List<String> orderTwo = List.of("C", "D");
        List<String> orderThree = List.of("D", "E");
        List<String> orderFour = List.of("F", "G");

        List<List<String>> input = new ArrayList<>();

        input.add(orderOne);
        input.add(orderTwo);
        input.add(orderThree);
        input.add(orderFour);

        main.consolidate(input);
    }

    @Test
    public void shouldConsolidateThree() {
        Main main = new Main();

        List<String> orderOne = List.of("A", "B", "C");
        List<String> orderTwo = List.of("E", "F", "G");
        List<String> orderThree = List.of("J", "K");
        List<String> orderFour = List.of("C", "D", "E");

        List<List<String>> input = new ArrayList<>();

        input.add(orderOne);
        input.add(orderTwo);
        input.add(orderThree);
        input.add(orderFour);

        main.consolidate(input);
    }

    @Test
    public void shouldConsolidateFour() {
        Main main = new Main();

        List<String> orderOne = List.of("A", "B", "C", "C");
        List<String> orderTwo = List.of();
        List<String> orderThree = List.of("A", "D", "D");
        List<String> orderFour = List.of("D", "E");
        List<String> orderFive = List.of("F", "G");

        List<List<String>> input = new ArrayList<>();

        input.add(orderOne);
        input.add(orderTwo);
        input.add(orderThree);
        input.add(orderFour);
        input.add(orderFive);

        main.consolidate(input);
    }
}
