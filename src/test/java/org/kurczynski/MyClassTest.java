package org.kurczynski;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MyClassTest {
    @Mock
    private MyClass mockMyClass;

    @Test
    void isAvailable() {
        when(mockMyClass.isAvailable()).thenReturn(false);

        assertFalse(this.mockMyClass.isAvailable());
    }
}
